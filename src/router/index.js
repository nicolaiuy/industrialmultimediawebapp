import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Ingreso',
    component: () => import('../views/Ingreso.vue'),
  },
  {
    path: '/activarcuenta',
    name: 'ActivarCuenta',
    component: () => import('../views/ActivarCuenta.vue'),
  },
  {
    path: '/hitos',
    name: 'Hitos',
    component: () => import('../views/Hitos.vue'),
  },
  {
    path: '/cuenta',
    name: 'Cuenta',
    component: () => import('../views/Cuenta.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;