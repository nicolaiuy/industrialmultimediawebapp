import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookies from 'vue-cookies'

import './custom.scss'

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

Vue.use(VueAxios, axios);
Vue.use(VueCookies);

const viewCookie = VueCookies.get('view');
if(viewCookie){
  store.state.view = viewCookie;
}

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
