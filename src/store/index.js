import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    view: "list"
  },
  mutations: {
    updateView (state, _view) {
      state.view = _view; 
    }
  },
  actions: {
    updateView(context, _view) {
      Vue.$cookies.set('view', _view);
      context.commit('updateView', _view)
    }
  },
});
